package task2;

import java.util.List;

public class Task2Test {

    public static void main(String[] args) {

        Integer[] numbers = new Integer[] { 2, 7, 9, 0, 1 };
        Convertor<Integer> convertorNumbers = new Convertor<>();
        List<Integer> listIntegers = convertorNumbers.convert(numbers);
        for (Integer value : listIntegers) {
            System.out.println(value);
        }

        System.out.println("-------------------");

        String[] strings = new String[] { "Бублик", "Крендель", "Кекс", "Пряник" };
        Convertor<String> convertorStrings = new Convertor<>();
        List<String> listStrings = convertorStrings.convert(strings);
        for (String value : listStrings) {
            System.out.println(value);
        }

    }

}
