package task4;

import java.util.*;

public class Task4Test {

    public static void main(String[] args) {

        List<Element> list = new ArrayList<>();
        list.add(new Element(1, "Яблоко"));
        list.add(new Element(2, "Яблоко"));
        list.add(new Element(3, "Яблоко"));

        list.add(new Element(4, "Персик"));
        list.add(new Element(5, "Персик"));

        list.add(new Element(6, "Груша"));
        list.add(new Element(7, "Груша"));

        list.add(new Element(8, "Лимон"));

        Map<String, List<Integer>> map = new HashMap<>();

        for (Element element : list) {
            List<Integer> integers = map.getOrDefault(element.getName(), new ArrayList<Integer>());
            integers.add(element.getId());
            map.put(element.getName(), integers);
        }

        System.out.println(map);

    }

}
