package task3;

import java.util.ArrayList;
import java.util.List;

public class Task3Test {

    public static void main(String[] args) {

        Box<Apple> appleBox = new Box<>("Коробка для яблок");
        Box<Orange> orangeBox = new Box<>("Коробка для апельсинов");
        int appleNumbers = 3;
        int orangeNumbers = 2;

        /* наполнение коробок */

        for (int j = 0; j < appleNumbers; j++) {
            appleBox.addFruit(new Apple());
        }

        for (int j = 0; j < orangeNumbers; j++) {
            orangeBox.addFruit(new Orange());
        }

        /* сравнение корбок */

        System.out.println("Коробки равны? " + appleBox.compare(orangeBox));
        System.out.println(appleBox);
        System.out.println(orangeBox);

        /* пересыпание */

        Box<Apple> anotoherBox = new Box<>("Новая коробка");

        List<Box> allBoxes = new ArrayList<>();
        allBoxes.add(appleBox);
        allBoxes.add(orangeBox);
        allBoxes.add(anotoherBox);

        /* вывод в порядке добавления */
        System.out.println(allBoxes);

        /* пересыпаем яблоки */
        appleBox.sprinke(anotoherBox);

        /* сортируем в убывающем порядке */
        allBoxes.sort((o1, o2) -> o2.getWeight().compareTo(o1.getWeight()));

        /* вывод списка корбок */
        System.out.println(allBoxes);


    }

}
