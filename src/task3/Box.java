package task3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Box<T extends Fruit> implements Comparable<Box<? extends Fruit>> {

    private String name;
    private List<T> fruits = new ArrayList<T>();

    public Box(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name + " (вес: " + getWeight() + ")";
    }

    public Float getWeight() {
        float weight = 0;

        for (T fruit : fruits) {
            weight += fruit.getWeight();
        }

        return weight;
    }

    public void addFruit(T fruit) {
        fruits.add(fruit);
    }

    public boolean compare(Box<? extends Fruit> anotherBox) {
        return (this.compareTo(anotherBox) == 0) ? true : false;
    }

    @Override
    public int compareTo(Box<? extends Fruit> anotherBox) {
        return this.getWeight().compareTo(anotherBox.getWeight());
    }

    public void sprinke(Box<T> anotherBox) {
        Iterator<T> iterator = fruits.iterator();

        while (iterator.hasNext()) {
            T next = iterator.next();
            anotherBox.addFruit(next);
            iterator.remove();
        }

    }
}

