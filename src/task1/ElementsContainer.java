package task1;

public class ElementsContainer<T> {

    private T[] elements;

    public ElementsContainer() {
    }

    public ElementsContainer(T[] elements) {
        this.elements = elements;
    }

    @Override
    public String toString() {
        if (elements == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        for (T element : elements) {
            sb.append(element);
            sb.append("\t");
        }
        return sb.toString();
    }

    public void swap(int index1, int index2) {
        if (elements == null || elements.length == 0) {
            throw new IllegalStateException("Массив пустой.");
        }

        if (index1 < 0 || index1 >= elements.length
                || index2 < 0 || index2 >= elements.length) {
            throw new IllegalArgumentException("Некорректные входные данные.");
        }

        if (index1 != index2) {
            T element1 = elements[ index1 ];
            T element2 = elements[ index2 ];
            elements[ index1 ] = element2;
            elements[ index2 ] = element1;

            System.out.println("Элементы " + element1 + " и " + element2 + " успешно обменялись местами.");
        }

    }
}
