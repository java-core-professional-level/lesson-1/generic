package task1;

public class Task1Test {

    public static void main(String[] args) {

        Integer[] integers = new Integer[] { 2, 7, 9, 1, 5 };
        ElementsContainer<Integer> containerOfInteger = new ElementsContainer<>(integers);

        System.out.println(containerOfInteger);
        containerOfInteger.swap(0, 4);
        System.out.println(containerOfInteger);

        System.out.println("--------------------------");

        String[] strings = new String[] { "яблоко", "персик", "кукуруза", "помидор", "щавель" };
        ElementsContainer<String> containerOfString = new ElementsContainer<>(strings);

        System.out.println(containerOfString);
        containerOfString.swap(1, 4);
        System.out.println(containerOfString);

    }



}
